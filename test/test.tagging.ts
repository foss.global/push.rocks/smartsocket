// tslint:disable-next-line:no-implicit-dependencies
import { expect, tap } from '@pushrocks/tapbundle';

import * as smartsocket from '../ts/index.js';

let testSmartsocket: smartsocket.Smartsocket;
let testSmartsocketClient: smartsocket.SmartsocketClient;
let testSocketFunctionForServer: smartsocket.SocketFunction<any>;
let testSocketFunctionClient: smartsocket.SocketFunction<any>;

export interface IReqResClient {
  method: 'testFunction1';
  request: {
    value1: string;
  };
  response: {
    value1: string;
  };
}

export interface IReqResServer {
  method: 'testFunction2';
  request: {
    hi: string;
  };
  response: {
    hi: string;
  };
}

const testConfig = {
  port: 3000,
};

// class smartsocket
tap.test('should create a new smartsocket', async () => {
  testSmartsocket = new smartsocket.Smartsocket({ alias: 'testserver2', port: testConfig.port });
  expect(testSmartsocket).toBeInstanceOf(smartsocket.Smartsocket);
});

// class SocketFunction
tap.test('should register a new Function', async () => {
  testSocketFunctionForServer = new smartsocket.SocketFunction({
    funcDef: async (dataArg, socketConnectionArg) => {
      return dataArg;
    },
    funcName: 'testFunction1',
  });
  testSmartsocket.addSocketFunction(testSocketFunctionForServer);

  testSocketFunctionClient = new smartsocket.SocketFunction({
    funcDef: async (dataArg, socketConnectionArg) => {
      return dataArg;
    },
    funcName: 'testFunction2',
  });
  testSmartsocket.addSocketFunction(testSocketFunctionForServer);
});

tap.test('should start listening when .started is called', async () => {
  await testSmartsocket.start();
});

// class SmartsocketClient
tap.test('should react to a new websocket connection from client', async () => {
  testSmartsocketClient = new smartsocket.SmartsocketClient({
    port: testConfig.port,
    url: 'http://localhost',
    alias: 'testClient1',
  });
  testSmartsocketClient.addSocketFunction(testSocketFunctionClient);
  await testSmartsocketClient.connect();
});

tap.test('should be able to tag a connection from client', async (tools) => {
  await testSmartsocketClient.addTag({
    id: 'awesome',
    payload: 'yes',
  });
  const tagOnServerSide = await testSmartsocket.socketConnections
    .findSync((socketConnection) => {
      return true;
    })
    .getTagById('awesome');
  expect(tagOnServerSide.payload).toEqual('yes');
});

tap.test('should be able to tag a connection from server', async (tools) => {
  await testSmartsocket.socketConnections
    .findSync((socketConnection) => {
      return true;
    })
    .addTag({
      id: 'awesome2',
      payload: 'absolutely',
    });
  const tagOnClientSide = await testSmartsocketClient.socketConnection.getTagById('awesome2');
  expect(tagOnClientSide.payload).toEqual('absolutely');
});

tap.test('2 clients should connect in parallel', async () => {
  // TODO: implement parallel test
});

tap.test('should be able to make a functionCall from client to server', async () => {
  const response = await testSmartsocketClient.serverCall<IReqResClient>('testFunction1', {
    value1: 'hello',
  });
  console.log(response);
  expect(response.value1).toEqual('hello');
});

tap.test('should be able to make a functionCall from server to client', async () => {
  const response = await testSmartsocket.clientCall<IReqResServer>(
    'testFunction2',
    {
      hi: 'hi there from server',
    },
    testSmartsocket.socketConnections.findSync((socketConnection) => {
      return true;
    })
  );
  console.log(response);
  expect(response.hi).toEqual('hi there from server');
});

tap.test('client should disconnect and reconnect', async (tools) => {
  await testSmartsocketClient.disconnect();
  await testSmartsocketClient.connect();
});

tap.test('should be able to locate a connection tag after reconnect', async (tools) => {
  console.log(testSmartsocket.socketConnections.getArray().length);
  const tagOnServerSide = await testSmartsocket.socketConnections
    .findSync((socketConnection) => {
      return true;
    })
    .getTagById('awesome');
  expect(tagOnServerSide.payload).toEqual('yes');
});

// terminate
tap.test('should close the server', async () => {
  await testSmartsocket.stop();
});

tap.start();
