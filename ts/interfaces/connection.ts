export interface IRequestAuthPayload {
  serverAlias: string;
}

export type TConnectionStatus =
  | 'new'
  | 'connecting'
  | 'connected'
  | 'disconnecting'
  | 'disconnected'
  | 'timedOut';
