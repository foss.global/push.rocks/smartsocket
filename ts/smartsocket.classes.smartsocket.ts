import * as plugins from './smartsocket.plugins.js';
import * as pluginsTyped from './smartsocket.pluginstyped.js';
import * as interfaces from './interfaces/index.js';

// classes
import { SocketConnection } from './smartsocket.classes.socketconnection.js';
import {
  ISocketFunctionCallDataRequest,
  SocketFunction,
  ISocketFunctionCallDataResponse,
} from './smartsocket.classes.socketfunction.js';
import { SocketRequest } from './smartsocket.classes.socketrequest.js';
import { SocketServer } from './smartsocket.classes.socketserver.js';

import { logger } from './smartsocket.logging.js';

export interface ISmartsocketConstructorOptions {
  alias: string;
  port?: number;
}

export class Smartsocket {
  /**
   * a unique id to detect server restarts
   */
  public alias: string;
  public smartenv = new plugins.smartenv.Smartenv();
  public options: ISmartsocketConstructorOptions;
  public io: pluginsTyped.socketIo.Server;
  public socketConnections = new plugins.lik.ObjectMap<SocketConnection>();
  public socketFunctions = new plugins.lik.ObjectMap<SocketFunction<any>>();
  public socketRequests = new plugins.lik.ObjectMap<SocketRequest<any>>();

  public eventSubject = new plugins.smartrx.rxjs.Subject<interfaces.TConnectionStatus>();

  private socketServer = new SocketServer(this);

  constructor(optionsArg: ISmartsocketConstructorOptions) {
    this.options = optionsArg;
    this.alias = plugins.isounique.uni(this.options.alias);
  }

  public async setExternalServer(serverType: 'smartexpress', serverArg: any) {
    await this.socketServer.setExternalServer(serverType, serverArg);
  }

  /**
   * starts smartsocket
   */
  public async start() {
    const socketIoModule = await this.smartenv.getSafeNodeModule('socket.io');
    this.io = new socketIoModule.Server(await this.socketServer.getServerForSocketIo(), {
      cors: {
        allowedHeaders: '*',
        methods: '*',
        origin: '*',
      }
    });
    await this.socketServer.start();
    this.io.on('connection', (socketArg) => {
      this._handleSocketConnection(socketArg);
    });
  }

  /**
   * stops smartsocket
   */
  public async stop() {
    await plugins.smartdelay.delayFor(1000);
    this.socketConnections.forEach((socketObjectArg: SocketConnection) => {
      if (socketObjectArg) {
        logger.log(
          'info',
          `disconnecting socket with >>alias ${socketObjectArg.alias} due to server stop...`
        );
        socketObjectArg.disconnect();
      }
    });
    this.socketConnections.wipe();
    this.io.close();

    // stop the corresponging server
    this.socketServer.stop();
  }

  // communication

  /**
   * allows call to specific client.
   */
  public async clientCall<T extends plugins.typedrequestInterfaces.ITypedRequest>(
    functionNameArg: T['method'],
    dataArg: T['request'],
    targetSocketConnectionArg: SocketConnection
  ): Promise<T['response']> {
    const socketRequest = new SocketRequest<T>(this, {
      funcCallData: {
        funcDataArg: dataArg,
        funcName: functionNameArg,
      },
      originSocketConnection: targetSocketConnectionArg,
      shortId: plugins.isounique.uni(),
      side: 'requesting',
    });
    const response: ISocketFunctionCallDataResponse<T> = await socketRequest.dispatch();
    const result = response.funcDataArg;
    return result;
  }

  public addSocketFunction(socketFunction: SocketFunction<any>) {
    this.socketFunctions.add(socketFunction);
  }

  /**
   * the standard handler for new socket connections
   */
  private async _handleSocketConnection(socketArg: pluginsTyped.socketIo.Socket) {
    const socketConnection: SocketConnection = new SocketConnection({
      alias: undefined,
      authenticated: false,
      side: 'server',
      smartsocketHost: this,
      socket: socketArg,
    });
    logger.log('info', 'Socket connected. Trying to authenticate...');
    this.socketConnections.add(socketConnection);
    const disconnectSubscription = socketConnection.eventSubject.subscribe((eventArg) => {
      if (eventArg === 'disconnected') {
        this.socketConnections.remove(socketConnection);
        disconnectSubscription.unsubscribe();
      }
    });
    await socketConnection.authenticate();
    await socketConnection.listenToFunctionRequests();
    await socketConnection.socket.emit('serverFullyReactive');
  }
}
